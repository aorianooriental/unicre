# Changelog

All Notable changes to `Acoriano/Unicre` will be documented in this file.

## 0.2.0 - 2017-05-18

### Added
- Support for language in the web payment request
- Support for buyer information in web payment request

## 0.1.0 - 2017-05-16

### Added
- First release 