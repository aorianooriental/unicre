Unicre connector library for Açoriano Oriental
==============================================

This library connects to redeunicre e-commerce payment API.


This package is compliant with PSR-2 code standards and PSR-4 autoload standards. It also applies the semantic version 2.0.0 specification.
