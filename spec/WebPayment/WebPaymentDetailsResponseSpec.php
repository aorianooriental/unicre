<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\WebPayment;

use Acoriano\Unicre\Domain\Authorization;
use Acoriano\Unicre\Domain\Payment;
use Acoriano\Unicre\Domain\Transaction;
use Acoriano\Unicre\WebPayment\Result;
use Acoriano\Unicre\WebPayment\WebPaymentDetailsResponse;
use PhpSpec\ObjectBehavior;

/**
 * WebPaymentDetailsResponse specification
 *
 * @package spec\Acoriano\Unicre\WebPayment
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class WebPaymentDetailsResponseSpec extends ObjectBehavior
{
    function let(
        Result $result,
        Authorization $authorization,
        Payment $payment,
        Transaction $transaction
    )
    {
        $this->beConstructedWith($result, $authorization, $payment, $transaction);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(WebPaymentDetailsResponse::class);
    }

    function it_has_a_response_result(Result $result)
    {
        $this->result()->shouldBe($result);
    }

    function it_has_an_authorization(
        Authorization $authorization
    )
    {
        $this->authorization()->shouldBe($authorization);
    }

    function it_has_a_payment(Payment $payment)
    {
        $this->payment()->shouldBe($payment);
    }

    function it_has_a_transaction(Transaction $transaction)
    {
        $this->transaction()->shouldBe($transaction);
    }
}
