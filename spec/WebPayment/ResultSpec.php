<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\WebPaymentResponse;

use Acoriano\Unicre\WebPayment\Result;
use PhpSpec\ObjectBehavior;

/**
 * Result specification
 *
 * @package spec\Acoriano\Unicre\WebPaymentResponse
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class ResultSpec extends ObjectBehavior
{

    function let()
    {
        $this->beConstructedWith('12342', 'Error', 'Long message');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Result::class);
    }

    function it_has_a_result_code()
    {
        $this->code()->shouldBe('12342');
    }

    function it_has_a_short_error_message()
    {
        $this->status()->shouldBe('Error');
    }

    function it_has_a_description()
    {
        $this->description()->shouldBe('Long message');
    }
}
