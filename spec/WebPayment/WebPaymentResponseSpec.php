<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\WebPayment;

use Acoriano\Unicre\Domain\Common\Url;
use Acoriano\Unicre\WebPayment\WebPaymentResponse;
use Acoriano\Unicre\WebPayment\Result;
use PhpSpec\ObjectBehavior;

/**
 * WebPaymentResponse specification
 *
 * @package spec\Acoriano\Unicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class WebPaymentResponseSpec extends ObjectBehavior
{
    function let(Result $result, Url $url)
    {
        $this->beConstructedWith($result, 'token', $url);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(WebPaymentResponse::class);
    }

    function it_has_a_result(
        Result $result
    )
    {
        $this->result()->shouldBe($result);
    }

    function it_has_an_identification_token()
    {
        $this->token()->shouldBe('token');
    }

    function it_has_an_url(Url $url)
    {
        $this->redirectUrl()->shouldBe($url);
    }
}
