<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\WebPayment;

use Acoriano\Unicre\Domain\Buyer;
use Acoriano\Unicre\Domain\Common\Language;
use Acoriano\Unicre\Domain\Common\Money;
use Acoriano\Unicre\Domain\Common\Url;
use Acoriano\Unicre\Domain\Order;
use Acoriano\Unicre\Domain\Payment;
use Acoriano\Unicre\WebPayment\WebPaymentRequest;
use PhpSpec\ObjectBehavior;

/**
 * WebPaymentRequest specification
 *
 * @package spec\Acoriano\Unicre\WebPayment
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class WebPaymentRequestSpec extends ObjectBehavior
{

    const CONTRACT = '00995902';

    function let(
        Order $order,
        Money $amount
    )
    {
        $order->amount()->willReturn($amount);
        $this->beConstructedWith(self::CONTRACT, $order);
    }

    function it_is_initializable_with_an_order()
    {
        $this->shouldHaveType(WebPaymentRequest::class);
    }

    function it_has_an_order(
        Order $order
    )
    {
        $this->order()->shouldBe($order);
    }

    function it_has_a_payment(
        Money $amount
    )
    {
        $this->payment()->shouldBeAnInstanceOf(Payment::class);
        $this->payment()->amount()->shouldBe($amount);
    }

    function it_can_set_the_return_success_url(
        Url $url
    )
    {
        $this->setReturnUrl($url)->shouldBe($this->getWrappedObject());
        $this->returnUrl()->shouldBe($url);
    }

    function it_can_set_the_cancel_url(
        Url $url
    )
    {
        $this->setCancelUrl($url)->shouldBe($this->getWrappedObject());
        $this->cancelUrl()->shouldBe($url);
    }

    function it_has_a_security_mode()
    {
        $this->securityMode()->shouldBe('SSL');
    }

    function it_has_recurring_information()
    {
        $this->recurring()->shouldBeArray();
    }

    function it_has_a_language()
    {
        $this->language()->shouldBeAnInstanceOf(Language::class);
        $this->language()->__toString()->shouldBe('eng');
    }

    function it_can_change_payment_language()
    {
        $this->changeLanguage(new Language(Language::PORTUGUESE))
            ->shouldBe($this->getWrappedObject());
        $this->language()->__toString()->shouldBe('pt');
    }

    function it_may_have_a_buyer(
        Buyer $buyer
    )
    {
        $this->setBuyer($buyer)->shouldBe($this->getWrappedObject());
        $this->buyer()->shouldBe($buyer);
        $this->hasBuyer()->shouldBe(true);
    }
}
