<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\RedUnicre;

use Acoriano\Unicre\Domain\Common\Money;
use Acoriano\Unicre\Domain\Common\Url;
use Acoriano\Unicre\Domain\Order;
use Acoriano\Unicre\Domain\Payment;
use Acoriano\Unicre\Domain\Transaction;
use Acoriano\Unicre\Exception\WebPaymentResponse\InvalidTransactionException;
use Acoriano\Unicre\RedUnicre\RedUnicreWebPayment;
use Acoriano\Unicre\WebPaymentGetaway;
use Acoriano\Unicre\WebPayment\WebPaymentRequest;
use Acoriano\Unicre\WebPayment\WebPaymentResponse;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * RedUnicreWebPayment specification
 *
 * @package spec\Acoriano\Unicre\RedUnicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class RedUnicreWebPaymentSpec extends ObjectBehavior
{

    function let(MockedClient $client)
    {
        $client->doWebPayment(Argument::type('array'))
            ->willReturn($this->response());
        $this->beConstructedWith($client);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(RedUnicreWebPayment::class);
    }

    function its_a_web_payment_gateway()
    {
        $this->shouldImplement(WebPaymentGetaway::class);
    }

    function it_calls_do_web_payment_soap_function(
        MockedClient $client
    )
    {
        $request = $this->paymentRequest();
        $this->doWebPayment($request)->shouldBeAnInstanceOf(WebPaymentResponse::class);
        $client->doWebPayment( [
            'payment' => [
                'amount' => 1200,
                'currency' => 978,
                'action' => '101',
                'mode' => 'CPT',
                'contractNumber' => '123456'
            ],
            'returnURL' => 'http://example.com/success',
            'cancelURL' => 'http://example.com/cancel',
            'languageCode' => 'eng',
            'order' => [
                'ref' => 'xxxfff-ddd',
                'amount' => 1200,
                'currency' => 978,
                "date" => $request->order()->date()->format('d/m/Y H:i')
            ],
            'securityMode' => 'SSL',
            'recurring' => [
                'amount' => '',
                'billingCycle' => '',
                'billingLeft' => ''
            ]
        ])->shouldHaveBeenCalled();
    }

    function it_throws_invalid_transaction_exception(
        MockedClient $client
    )
    {
        $request = $this->paymentRequest();
        $client->doWebPayment(Argument::type('array'))
            ->willReturn($this->response('02303'));
        $this->shouldThrow(InvalidTransactionException::class)
            ->during('doWebPayment', [$request]);
    }

    function it_request_capture_of_authorized_payment(
        Transaction $transaction,
        MockedClient $client
    )
    {
        $transaction->transactionId()->willReturn('12345');
        $payment = new Payment('12312', Money::create(12, 'EUR'));
        $payment->changeAction(Payment::ACTION_VALIDATION);
        $client->doCapture([
            'transactionId' => '12345',
            'payment' => [
                'amount' => $payment->amount()->forPayment(),
                'currency' => $payment->amount()->currency()->number(),
                'action' => $payment->action(),
                'mode' => $payment->mode(),
                'contractNumber' => $payment->contractNumber()
            ]
        ])->shouldBeCalled();
        $this->doCapture($transaction, $payment);

    }

    private function paymentRequest()
    {
        $request = new WebPaymentRequest('123456',
            new Order('xxxfff-ddd', Money::create(12, 'EUR'))
        );
        $request->setReturnUrl(new Url('http://example.com/success'))
            ->setCancelUrl(new Url('http://example.com/cancel'));
        return $request;
    }

    private function response($code = '00000')
    {
        return (object) [
            'result' => (object) [
                'code' => $code,
                'shortMessage' => 'ACCEPTED',
                'longMessage' => 'Transaction approved'
            ],
            'token' => '24QfnSZAi3yCiosji2721494693831218',
            'redirectURL' => 'https://teste-pagamentosweb.redunicre.pt/webpayment/step2.do?reqCode=prepareStep2&token=24QfnSZAi3yCiosji2721494693831218'
        ];
    }
}

class MockedClient extends \SoapClient
{

    public function doWebPayment($request)
    {
        return [];
    }

    public function getWebPaymentDetails($request)
    {
        return [];
    }

    public function doCapture(array $request)
    {
        return [];
    }
}
