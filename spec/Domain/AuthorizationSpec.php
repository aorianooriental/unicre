<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Authorization;
use PhpSpec\ObjectBehavior;

/**
 * Authorization specification
 *
 * @package spec\Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class AuthorizationSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('A55A', '13/05/2017 18:33');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Authorization::class);
    }

    function it_has_a_number()
    {
        $this->number()->shouldBe('A55A');
    }

    function it_has_a_date()
    {
        $this->date()->shouldBeAnInstanceOf(\DateTimeImmutable::class);
        $this->date()->format('Y-m-d H:i')->shouldBe('2017-05-13 18:33');
    }
}
