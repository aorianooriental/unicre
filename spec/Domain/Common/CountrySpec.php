<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Country;
use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\UnknownOrInvalidCountryException;
use PhpSpec\ObjectBehavior;

/**
 * Country specification
 *
 * @package spec\Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class CountrySpec extends ObjectBehavior
{

    function let()
    {
        $this->beConstructedWith(Country::PORTUGAL);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Country::class);
    }

    function it_can_be_converted_to_string()
    {
        $this->shouldImplement(Stringable::class);
        $this->__toString()->shouldBe('PT');
    }

    function it_throws_an_Exception_for_invalid_or_unknown_codes()
    {
        $this->beConstructedWith('xbf');
        $this->shouldThrow(UnknownOrInvalidCountryException::class)
            ->duringInstantiation();
    }
}
