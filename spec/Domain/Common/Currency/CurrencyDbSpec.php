<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain\Common\Currency;

use Acoriano\Unicre\Domain\Common\Currency\CurrencyDb;
use PhpSpec\ObjectBehavior;

/**
 * CurrencyDb specification
 *
 * @package spec\Acoriano\Unicre\Domain\Common\Currency
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class CurrencyDbSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('instance');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CurrencyDb::class);
    }

    function it_is_only_created_once()
    {
        $expected = CurrencyDb::instance();
        $this->shouldBeAnInstanceOf(get_class($expected));
    }

    function it_can_return_currency_information_from_iso_code()
    {
        $this->withCode('EUR')['name']->shouldBe('Euro');
    }

    function it_can_return_currency_information_from_iso_number()
    {
        $this->withCode(978)['name']->shouldBe('Euro');
        $this->withCode(360)['name']->shouldBe('Rupiah');
    }
}
