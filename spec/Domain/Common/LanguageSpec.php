<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Language;
use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\UnknownOrInvalidLanguageException;
use PhpSpec\ObjectBehavior;

/**
 * Language specification
 *
 * @package spec\Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class LanguageSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(Language::FRENCH);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Language::class);
    }

    function it_can_be_casted_to_string()
    {
        $this->shouldImplement(Stringable::class);
        $this->__toString()->shouldBe(Language::FRENCH);
    }

    function it_throws_an_exception_for_unknown_languages()
    {
        $this->beConstructedWith('test language');
        $this->shouldThrow(UnknownOrInvalidLanguageException::class)
            ->duringInstantiation();
    }
}
