<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Email;
use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\InvalidEmailAddressException;
use PhpSpec\ObjectBehavior;

/**
 * Email specification
 *
 * @package spec\Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class EmailSpec extends ObjectBehavior
{

    function let()
    {
        $this->beConstructedWith('john.doe@example.com');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Email::class);
    }

    function it_can_be_converted_to_string()
    {
        $this->shouldImplement(Stringable::class);
        $this->__toString()->shouldBe('john.doe@example.com');
    }

    function it_throws_an_exception_when_created_with_invalid_addresses()
    {
        $this->beConstructedWith('a silly e-mail address');
        $this->shouldThrow(InvalidEmailAddressException::class)
            ->duringInstantiation();
    }
}
