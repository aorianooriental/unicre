<?php

namespace spec\Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Currency;
use Acoriano\Unicre\Domain\Common\Money;
use PhpSpec\ObjectBehavior;

class MoneySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(123.434, new Currency('EUR'));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Money::class);
    }

    function it_has_a_currency()
    {
        $this->currency()->shouldBeAnInstanceOf(Currency::class);
        $this->currency()->code()->shouldBe('EUR');
    }

    function it_has_a_value()
    {
        $this->value()->shouldBe(123.43);
    }

    function it_can_be_converted_to_integer_for_payments()
    {
        $this->forPayment()->shouldBe(12343);
    }

    function it_can_be_created_statically()
    {
        $this->beConstructedThrough('create', [12.167, 'USD']);
        $this->value()->shouldBe(12.17);
        $this->currency()->code()->shouldBe('USD');
    }

}
