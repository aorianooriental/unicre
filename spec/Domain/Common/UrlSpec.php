<?php

namespace spec\Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Url;
use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\InvalidUrlException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UrlSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('http://www.example.com/test');
    }

    function it_is_initializable_with_an_url()
    {
        $this->shouldHaveType(Url::class);
    }

    function it_throws_exception_when_url_is_invalid()
    {
        $this->beConstructedWith('an invalid url');
        $this->shouldThrow(InvalidUrlException::class)->duringInstantiation();
    }

    function it_can_be_converted_to_string()
    {
        $this->shouldImplement(Stringable::class);
        $this->__toString()->shouldBe('http://www.example.com/test');
    }

}
