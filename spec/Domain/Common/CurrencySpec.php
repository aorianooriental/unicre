<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Currency;
use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\InvalidCurrencyCodeException;
use PhpSpec\ObjectBehavior;

/**
 * Currency specification
 *
 * @package spec\Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class CurrencySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('eur');
    }

    function it_is_initializable_with_an_iso_4217_code()
    {
        $this->shouldHaveType(Currency::class);
    }

    function it_throws_an_exception_for_an_unknown_currency_code()
    {
        $this->beConstructedWith('unknown');
        $this->shouldThrow(InvalidCurrencyCodeException::class)
            ->duringInstantiation();
    }

    function it_has_a_code()
    {
        $this->code()->shouldBe('EUR');
    }

    function it_has_a_name()
    {
        $this->name()->shouldBe('Euro');
    }

    function it_has_a_number()
    {
        $this->number()->shouldBe(978);
    }

    function it_has_a_list_of_locations_using_this_currency()
    {
        $this->locations()->shouldBe([
            "Åland Islands",
            "Andorra",
            "Austria",
            "Belgium",
            "Cyprus",
            "Estonia",
            "European Union",
            "Finland",
            "France",
            "French Guiana",
            "French Southern Territories (the)",
            "Germany",
            "Greece",
            "Guadeloupe",
            "Holy See (the)",
            "Ireland",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Martinique",
            "Mayotte",
            "Monaco",
            "Montenegro",
            "Netherlands (the)",
            "Portugal",
            "RÉunion",
            "Saint BarthÉlemy",
            "Saint Martin (french Part)",
            "Saint Pierre And Miquelon",
            "San Marino",
            "Slovakia",
            "Slovenia",
            "Spain"
        ]);
    }

    function it_can_be_converted_to_string()
    {
        $this->shouldImplement(Stringable::class);
        $this->__toString()->shouldBe('EUR');
    }
}
