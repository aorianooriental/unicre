<?php

namespace spec\Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Common\Money;
use Acoriano\Unicre\Domain\Payment;
use PhpSpec\ObjectBehavior;

class PaymentSpec extends ObjectBehavior
{

    const CONTRACT = '00995902';

    function let()
    {
        $this->beConstructedWith(
            self::CONTRACT,
            Money::create(125, 'EUR')
        );
    }

    function it_is_initializable_with_a_contract_and_a_money_amount()
    {
        $this->shouldHaveType(Payment::class);
    }

    function it_has_a_contract_number()
    {
        $this->contractNumber()->shouldBe(self::CONTRACT);
    }

    function it_has_an_amount()
    {
        $this->amount()->shouldBeAnInstanceOf(Money::class);
        $this->amount()->value()->shouldBe(125.00);
    }

    function it_has_an_action()
    {
        $this->action()->shouldBe(Payment::ACTION_AUTH_VALID);
    }

    function it_has_a_mode()
    {
        $this->mode()->shouldBe('CPT');
    }

    function it_can_change_action()
    {
        $this->changeAction(Payment::ACTION_VALIDATION)
            ->shouldBe($this->getWrappedObject());
    }

}
