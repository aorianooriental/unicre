<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Transaction;
use PhpSpec\ObjectBehavior;

/**
 * Transaction specification
 *
 * @package spec\Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class TransactionSpec extends ObjectBehavior
{

    function let()
    {
        $this->beConstructedWith('27133193340175', '13/05/2017 18:33');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Transaction::class);
    }

    function it_has_an_id()
    {
        $this->transactionId()->shouldBe('27133193340175');
    }

    function it_has_a_date()
    {
        $this->date()->shouldBeAnInstanceOf(\DateTimeImmutable::class);
        $this->date()->format('Y-m-d H:i')->shouldBe('2017-05-13 18:33');
    }
}
