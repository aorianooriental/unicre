<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Common\Money;
use Acoriano\Unicre\Domain\Order;
use PhpSpec\ObjectBehavior;

/**
 * Order specification
 *
 * @package spec\Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class OrderSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('123123123123', Money::create(120, 'EUR'));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Order::class);
    }

    function it_has_a_reference()
    {
        $this->reference()->shouldBe('123123123123');
    }

    function it_has_an_amount()
    {
        $this->amount()->shouldBeAnInstanceOf(Money::class);
        $this->amount()->value()->shouldBe(120.00);
        $this->amount()->currency()->code()->shouldBe('EUR');
    }

    function it_has_a_date()
    {
        $this->date()->shouldBeAnInstanceOf(\DateTimeImmutable::class);
    }
}
