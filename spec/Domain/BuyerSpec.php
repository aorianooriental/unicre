<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace spec\Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Buyer;
use Acoriano\Unicre\Domain\Common\Email;
use PhpSpec\ObjectBehavior;

/**
 * Buyer specification
 *
 * @package spec\Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class BuyerSpec extends ObjectBehavior
{

    function let(Email $email)
    {
        $this->beConstructedWith('John Doe', $email);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Buyer::class);
    }

    function it_has_an_email_address(Email $email)
    {
        $this->email()->shouldBe($email);
    }

    function it_has_a_name()
    {
        $this->name()->shouldBe('John Doe');
    }

    function it_has_a_first_name()
    {
        $this->firstName()->shouldBe('John');
    }

    function it_has_a_last_name()
    {
        $this->lastName()->shouldBe('Doe');
    }
}
