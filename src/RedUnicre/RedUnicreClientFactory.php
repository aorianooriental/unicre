<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\RedUnicre;

/**
 * RedUnicreClientFactory
 *
 * @package Acoriano\Unicre\RedUnicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
final class RedUnicreClientFactory
{
    /**
     * @var array
     */
    private $settings;

    /**
     * Creates a RedUnicreClientFactory
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function createClient()
    {
        $data = base64_encode("{$this->settings['merchantId']}:{$this->settings['accessKey']}");
        $http = [
            'http' => [
                'header' =>  "Authorization: Basic {$data}\r\n"
                ]
        ];
        $context = stream_context_create($http);
        return new \SoapClient(
            $this->settings['wsdl'],
            [
                "stream_context" => $context
            ]
        );
    }


}