<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\RedUnicre;

use Acoriano\Unicre\Domain\Authorization;
use Acoriano\Unicre\Domain\Common\Money;
use Acoriano\Unicre\Domain\Common\Url;
use Acoriano\Unicre\Domain\Payment;
use Acoriano\Unicre\Domain\Transaction;
use Acoriano\Unicre\Exception\WebPaymentResponse\InternalErrorException;
use Acoriano\Unicre\Exception\WebPaymentResponse\InvalidTransactionException;
use Acoriano\Unicre\Exception\WebPaymentResponse\TransactionRefusedException;
use Acoriano\Unicre\WebPayment\Result;
use Acoriano\Unicre\WebPayment\WebPaymentDetailsResponse;
use Acoriano\Unicre\WebPayment\WebPaymentRequest;
use Acoriano\Unicre\WebPayment\WebPaymentResponse;
use Acoriano\Unicre\WebPaymentGetaway;

/**
 * RedUnicreWebPayment
 *
 * @package Acoriano\Unicre\RedUnicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class RedUnicreWebPayment implements WebPaymentGetaway
{
    /**
     * @var \SoapClient|UnicreSoapClient
     */
    private $client;

    /**
     * Creates a RedUnicreWebPayment
     *
     * @param \SoapClient $client
     */
    public function __construct(\SoapClient $client)
    {
        $this->client = $client;
    }

    /**
     * Does a web payment request
     *
     * @param WebPaymentRequest $request
     *
     * @return WebPaymentResponse
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    public function doWebPayment(WebPaymentRequest $request)
    {
        $data = $this->client->doWebPayment($this->createRequestData($request));
        $result = $this->generateResult($data);

        $this->checkResult($result);

        return new WebPaymentResponse($result, $data->token, new Url($data->redirectURL));
    }

    /**
     * Retrieves the details of the payment with provided token
     *
     * @param string $token
     *
     * @return WebPaymentDetailsResponse
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    public function getWebPaymentDetails($token)
    {
        $data = $this->client->getWebPaymentDetails(['token' => $token]);
        $result = $this->generateResult($data);

        $this->checkResult($result);

        return new WebPaymentDetailsResponse(
            $result,
            $this->generateAuthorization($data),
            $this->generatePayment($data),
            $this->generateTransaction($data)
        );
    }

    /**
     * Check result status
     *
     * @param Result $result
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    private function checkResult(Result $result)
    {
        if (preg_match('/^023\d\d$/i', trim($result->code()))) {
            throw new InvalidTransactionException($result->description());
        }
    }

    /**
     * Creates the request data for doWebPaymentRequest()
     *
     * @param WebPaymentRequest $request
     * @return array
     */
    private function createRequestData(WebPaymentRequest $request)
    {
        $data = [
            'payment' => [
                'amount' => $request->payment()->amount()->forPayment(),
                'currency' => $request->payment()->amount()->currency()->number(),
                'action' => $request->payment()->action(),
                'mode' => $request->payment()->mode(),
                'contractNumber' => $request->payment()->contractNumber()
            ],
            'returnURL' => (string) $request->returnUrl(),
            'cancelURL' => (string) $request->cancelUrl(),
            'languageCode' => (string) $request->language(),
            'order' => [
                'ref' => $request->order()->reference(),
                'amount' => $request->order()->amount()->forPayment(),
                'currency' => $request->order()->amount()->currency()->number(),
                'date' => $request->order()->date()->format('d/m/Y H:i')
            ],
            'securityMode' => $request->securityMode(),
            'recurring' => $request->recurring()
        ];

        if ($request->hasBuyer()) {
            $data['buyer'] = [
                'firstName' => $request->buyer()->firstName(),
                'lastName' => $request->buyer()->lastName(),
                'email' => (string) $request->buyer()->email()
            ];
        }

        return $data;
    }

    /**
     * Creates a result response from response data
     *
     * @param object $data
     *
     * @return Result
     */
    private function generateResult($data)
    {
        return new Result(
            $data->result->code,
            $data->result->shortMessage,
            $data->result->longMessage
        );
    }

    /**
     * Creates the payment for response data
     *
     * @param object $data
     *
     * @return Payment
     */
    private function generatePayment($data)
    {
        return new Payment(
            $data->payment->contractNumber,
            Money::create(
                (float) $data->payment->amount,
                (int) $data->payment->currency
            ),
            $data->payment->action
        );
    }

    /**
     * Creates the provider transaction from response data
     *
     * @param object $data
     *
     * @return Transaction
     */
    private function generateTransaction($data)
    {
        return new Transaction(
            $data->transaction->id,
            $data->transaction->date
        );
    }

    /**
     * Creates the payment authorization from response data
     *
     * @param object $data
     *
     * @return Authorization
     */
    private function generateAuthorization($data)
    {
        return new Authorization(
            $data->authorization->number,
            $data->authorization->date
        );
    }

    /**
     * Requests validation of an accepted authorization
     *
     * @param Transaction $transaction
     * @param Payment $payment
     *
     * @return mixed
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    public function doCapture(Transaction $transaction, Payment $payment)
    {
        $payment->changeAction(Payment::ACTION_VALIDATION);
        $request = [
            'transactionId' => $transaction->transactionId(),
            'payment' => [
                'amount' => $payment->amount()->forPayment(),
                'currency' => $payment->amount()->currency()->number(),
                'action' => $payment->action(),
                'mode' => $payment->mode(),
                'contractNumber' => $payment->contractNumber()
            ]
        ];
        return $this->client->doCapture($request);

    }
}