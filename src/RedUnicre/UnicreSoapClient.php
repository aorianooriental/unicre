<?php

/**
 * This file is part of Unicre
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\RedUnicre;

use Acoriano\Unicre\WebPayment\WebPaymentRequest;
use Acoriano\Unicre\WebPayment\WebPaymentResponse;

/**
 * UnicreSoapClient
 *
 * @package Acoriano\Unicre\RedUnicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
interface UnicreSoapClient
{

    /**
     * Request a new payment
     *
     * @param array $request
     *
     * @return object
     */
    public function doWebPayment(array $request);

    /**
     * Requests the details of a given payment
     *
     * @param array $request
     *
     * @return object
     */
    public function getWebPaymentDetails(array $request);

    /**
     * Requests validation of an accepted authorization
     *
     * @param array $request
     *
     * @return object
     */
    public function doCapture(array $request);
}