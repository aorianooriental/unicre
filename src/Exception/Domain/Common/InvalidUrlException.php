<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Exception\Domain\Common;

use Acoriano\Unicre\Exception;
use InvalidArgumentException;

/**
 * InvalidUrlException
 *
 * @package Acoriano\Unicre\Exception\Domain\Common
 */
class InvalidUrlException extends InvalidArgumentException implements Exception
{

}