<?php

/**
 * This file is part of Unicre
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Exception\Domain\Common;

use Acoriano\Unicre\Exception;
use InvalidArgumentException;

/**
 * InvalidEmailAddressException
 *
 * @package Acoriano\Unicre\Exception\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class InvalidEmailAddressException extends InvalidArgumentException implements
    Exception
{

}
