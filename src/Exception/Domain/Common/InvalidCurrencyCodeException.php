<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Exception\Domain\Common;

use Acoriano\Unicre\Exception;
use InvalidArgumentException;

/**
 * InvalidCurrencyCodeException
 *
 * @package Acoriano\Unicre\Exception\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class InvalidCurrencyCodeException extends InvalidArgumentException implements Exception
{

}
