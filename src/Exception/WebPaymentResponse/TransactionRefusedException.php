<?php

/**
 * This file is part of Unicre
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Exception\WebPaymentResponse;

use Acoriano\Unicre\Exception;

/**
 * TransactionRefusedException
 *
 * @package Acoriano\Unicre\Exception\WebPaymentResponse
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class TransactionRefusedException extends AbstractTransactionException implements Exception
{

}
