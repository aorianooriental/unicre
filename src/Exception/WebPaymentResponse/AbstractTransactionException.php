<?php

/**
 * This file is part of Unicre
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Exception\WebPaymentResponse;

use RuntimeException;

/**
 * AbstractTransactionException
 *
 * @package Acoriano\Unicre\Exception\WebPaymentResponse
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class AbstractTransactionException extends RuntimeException
{

}
