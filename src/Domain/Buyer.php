<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Common\Email;

/**
 * Buyer
 *
 * @package Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Buyer
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * Creates a Buyer
     *
     * @param string $name
     * @param Email $email
     */
    public function __construct($name, Email $email)
    {
        $this->name = $name;
        $this->email = $email;
        $this->splitNames();
    }

    /**
     * Buyer's e-mail address
     *
     * @return Email
     */
    public function email()
    {
        return $this->email;
    }

    /**
     * Buyer's name
     *
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Buyer's first name
     *
     * @return string
     */
    public function firstName()
    {
        return $this->firstName;
    }

    /**
     * Buyer's last name
     *
     * @return string
     */
    public function lastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the first and last names
     */
    private function splitNames()
    {
        $parts = explode(' ', $this->name());

        $this->firstName = trim(reset($parts));
        if (count($parts) > 1) {
            $this->lastName = trim(end($parts));
        }
    }

}