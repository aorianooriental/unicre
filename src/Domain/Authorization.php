<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain;

/**
 * Authorization
 *
 * @package Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Authorization
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var \DateTimeImmutable
     */
    private $date;


    /**
     * Creates a Authorization
     *
     * @param string $number
     * @param string $date
     */
    public function __construct($number, $date)
    {
        $this->number = $number;
        $this->date = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $date);
    }

    /**
     * Returns the authorization number
     *
     * @return string
     */
    public function number()
    {
        return $this->number;
    }

    /**
     * Gets the authorization date
     *
     * @return \DateTimeImmutable
     */
    public function date()
    {
        return $this->date;
    }


}