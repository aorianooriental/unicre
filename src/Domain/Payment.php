<?php

namespace Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Common\Money;

class Payment
{

    const ACTION_AUTH          = '100';
    const ACTION_AUTH_VALID    = '101';
    const ACTION_VALIDATION    = '201';
    const ACTION_REIMBURSEMENT = '421';

    /**
     * @var string
     */
    private $contractNumber;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $mode = 'CPT';

    /**
     * Payment
     *
     * @param string $contractNumber
     * @param Money  $amount
     * @param string $action
     */
    public function __construct(
        $contractNumber,
        Money $amount,
        $action = self::ACTION_AUTH_VALID
    )
    {
        $this->contractNumber = $contractNumber;
        $this->amount = $amount;
        $this->action = $action;
    }

    /**
     * Returns the contract number
     *
     * @return string
     */
    public function contractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * Payment amount
     *
     * @return Money
     */
    public function amount()
    {
        return $this->amount;
    }

    /**
     * Payment action
     *
     * @return string
     */
    public function action()
    {
        return $this->action;
    }

    /**
     * Payment mode
     *
     * @return string
     */
    public function mode()
    {
        return $this->mode;
    }

    /**
     * Change the payment action
     *
     * @param string $action
     *
     * @return Payment
     */
    public function changeAction($action)
    {
        $this->action = $action;
        return $this;
    }
}
