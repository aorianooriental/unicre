<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Common\Currency\CurrencyDb;
use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\InvalidCurrencyCodeException;

/**
 * Currency
 *
 * @package Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Currency implements Stringable
{

    /**
     * @var string;
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var string[]
     */
    private $locations;

    /**
     * Creates a Currency
     *
     * @param string|int $codeOrNumber
     */
    public function __construct($codeOrNumber)
    {
        $data = CurrencyDb::instance()->withCode($codeOrNumber);
        if (! is_array($data)) {
            throw new InvalidCurrencyCodeException(
                "Trying to create a currency with an invalid code. Use " .
                "ISO 4217 currency codes or numbers."
            );
        }

        $this->code = $data['code'];
        $this->name = $data['name'];
        $this->number = $data['number'];
        $this->locations = $data['locations'];
    }

    /**
     * Return the ISO 4217 currency code
     *
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * Returns the currency name
     *
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Returns the ISO 4217 currency number
     *
     * @return int
     */
    public function number()
    {
        return $this->number;
    }

    /**
     * Get the list of locations using this currency
     *
     * @return string[]
     */
    public function locations()
    {
        return $this->locations;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->code();
    }
}