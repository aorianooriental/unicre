<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\InvalidEmailAddressException;

/**
 * Email
 *
 * @package Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Email implements Stringable
{
    /**
     * @var string
     */
    private $emailAddress;

    /**
     * Creates a Email
     *
     * @param string $emailAddress
     */
    public function __construct($emailAddress)
    {
        if (! filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailAddressException(
                "Trying to create an e-mail object with an invalid address."
            );
        }
        $this->emailAddress = $emailAddress;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->emailAddress;
    }
}