<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\InvalidUrlException;

/**
 * Url
 *
 * @package Acoriano\Unicre\Domain\Common
 */
class Url implements Stringable
{
    /**
     * @var string
     */
    private $url;

    /**
     * Creates an URL
     *
     * @param string $url
     */
    public function __construct($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new InvalidUrlException(
                "Trying to create an URL object with an invalid or " .
                "malformed string."
            );
        }
        $this->url = $url;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->url;
    }
}
