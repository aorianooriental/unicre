<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\UnknownOrInvalidCountryException;

/**
 * Country
 *
 * @package Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Country implements Stringable
{

    const PORTUGAL = 'PT';
    const FRANCE = 'FR';
    const GERMANY = 'DE';
    const UNITED_KINGDOM = 'GB';
    const SPAIN = 'ES';
    const ITALY = 'IT';

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var array
     */
    private static $knownCodes;

    /**
     * Creates a Country
     *
     * @param string $countryCode
     */
    public function __construct($countryCode)
    {
        if (! in_array($countryCode, self::knownCodes())) {
            $valid = implode(', ', self::knownCodes());
            throw new UnknownOrInvalidCountryException(
                "Trying to create a language object with an unknown or " .
                "invalid code. Valid codes are: {$valid}"
            );
        }
        $this->countryCode = $countryCode;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->countryCode;
    }

    /**
     * Get the list of available or known country codes
     *
     * @return array
     */
    private static function knownCodes()
    {
        if (null === self::$knownCodes) {
            $reflection = new \ReflectionClass(__CLASS__);
            self::$knownCodes = $reflection->getConstants();
        }
        return self::$knownCodes;
    }
}