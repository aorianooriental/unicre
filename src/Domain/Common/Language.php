<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common;

use Acoriano\Unicre\Domain\Stringable;
use Acoriano\Unicre\Exception\Domain\Common\UnknownOrInvalidLanguageException;
use ReflectionClass;

/**
 * Language
 *
 * @package Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Language implements Stringable
{

    const FRENCH     = 'fra';
    const GERMAN     = 'ger';
    const ENGLISH    = 'eng';
    const SPANISH    = 'spa';
    const ITALIAN    = 'ita';
    const PORTUGUESE = 'pt';

    /**
     * @var array
     */
    private static $knownLanguages;

    /**
     * @var string
     */
    private $code;

    /**
     * Creates a Language
     *
     * @param string $code
     */
    public function __construct($code)
    {
        if (!in_array($code, self::knownLanguages())) {
            $valid = implode(', ', self::knownLanguages());
            throw new UnknownOrInvalidLanguageException(
                "Trying to create a language object with an unknown or " .
                "invalid code. Valid codes are: {$valid}"
            );
        }

        $this->code = $code;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->code;
    }

    /**
     * Get the list of known language codes
     *
     * @return array
     */
    private static function knownLanguages()
    {
        if (null === self::$knownLanguages) {
            $reflection = new ReflectionClass(Language::class);
            self::$knownLanguages = $reflection->getConstants();
        }
        return self::$knownLanguages;
    }
}