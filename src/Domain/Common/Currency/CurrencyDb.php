<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common\Currency;

/**
 * CurrencyDb
 *
 * @package Acoriano\Unicre\Domain\Common\Currency
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class CurrencyDb
{

    /**
     * @var CurrencyDb
     */
    private static $instance;

    /**
     * @var array
     */
    private $database;

    /**
     * @var array Common codes
     */
    private $commonCodes = [
        978 => 'EUR',
        840 => 'USD',
        124 => 'CAD',
        826 => 'GBP'
    ];

    /**
     * Imports the currency database
     */
    private function __construct()
    {
        $this->database = include (__DIR__.'/currencies.php');
    }

    private function __clone()
    {
        // Clone is not allowed. This is a singleton!
    }

    /**
     * Returns the instance of the currency database
     *
     * @return CurrencyDb
     */
    public static function instance()
    {
        if (null === self::$instance) {
            self::$instance = new CurrencyDb();
        }
        return self::$instance;
    }

    /**
     * Searches for currency data that match provided ISO code/number
     *
     * @param int|string $currencyCode
     *
     * @return array|null
     */
    public function withCode($currencyCode)
    {
        // Its an iso number try search
        if (is_integer($currencyCode)) {
            return $this->search($currencyCode);
        }

        // Its a ISO code, return it
        $currencyCode = strtoupper($currencyCode);
        if (array_key_exists($currencyCode, $this->database)) {
            return $this->database[$currencyCode];
        }

        // Not found
        return null;
    }

    /**
     * Searches database for the currency with matching ISO number
     *
     * @param int $number
     *
     * @return array|null
     */
    private function search($number)
    {
        if (array_key_exists($number, $this->commonCodes)) {
            return $this->database[$this->commonCodes[$number]];
        }

        foreach ($this->database as $currency) {
            if ($currency['number'] == $number) {
                return $currency;
            }
        }

        return null;
    }

}