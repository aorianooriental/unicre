<?php

/**
 * Currency ISO 4217 database
 * 
 * @see https://en.wikipedia.org/wiki/ISO_4217
 * Published: January 01, 2017
 */

return $currencyCodes = [
    'AFN' => [
        'name' => 'Afghani',
        'code' => 'AFN',
        'number' => 971,
        'locations' => [
            "Afghanistan"
        ]
    ],
    'EUR' => [
        'name' => 'Euro',
        'code' => 'EUR',
        'number' => 978,
        'locations' => [
            "Åland Islands",
            "Andorra",
            "Austria",
            "Belgium",
            "Cyprus",
            "Estonia",
            "European Union",
            "Finland",
            "France",
            "French Guiana",
            "French Southern Territories (the)",
            "Germany",
            "Greece",
            "Guadeloupe",
            "Holy See (the)",
            "Ireland",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Martinique",
            "Mayotte",
            "Monaco",
            "Montenegro",
            "Netherlands (the)",
            "Portugal",
            "RÉunion",
            "Saint BarthÉlemy",
            "Saint Martin (french Part)",
            "Saint Pierre And Miquelon",
            "San Marino",
            "Slovakia",
            "Slovenia",
            "Spain"
        ]
    ],
    'ALL' => [
        'name' => 'Lek',
        'code' => 'ALL',
        'number' => 8,
        'locations' => [
            "Albania"
        ]
    ],
    'DZD' => [
        'name' => 'Algerian Dinar',
        'code' => 'DZD',
        'number' => 12,
        'locations' => [
            "Algeria"
        ]
    ],
    'USD' => [
        'name' => 'US Dollar',
        'code' => 'USD',
        'number' => 840,
        'locations' => [
            "American Samoa",
            "Bonaire, Sint Eustatius And Saba",
            "British Indian Ocean Territory (the)",
            "Ecuador",
            "El Salvador",
            "Guam",
            "Haiti",
            "Marshall Islands (the)",
            "Micronesia (federated States Of)",
            "Northern Mariana Islands (the)",
            "Palau",
            "Panama",
            "Puerto Rico",
            "Timor-leste",
            "Turks And Caicos Islands (the)",
            "United States Minor Outlying Islands (the)",
            "United States Of America (the)",
            "Virgin Islands (british)",
            "Virgin Islands (u.s.)"
        ]
    ],
    'AOA' => [
        'name' => 'Kwanza',
        'code' => 'AOA',
        'number' => 973,
        'locations' => [
            "Angola"
        ]
    ],
    'XCD' => [
        'name' => 'East Caribbean Dollar',
        'code' => 'XCD',
        'number' => 951,
        'locations' => [
            "Anguilla",
            "Antigua And Barbuda",
            "Dominica",
            "Grenada",
            "Montserrat",
            "Saint Kitts And Nevis",
            "Saint Lucia",
            "Saint Vincent And The Grenadines"
        ]
    ],
    '' => [
        'name' => 'No universal currency',
        'code' => '',
        'number' => 0,
        'locations' => [
            "Antarctica",
            "Palestine, State Of",
            "South Georgia And The South Sandwich Islands"
        ]
    ],
    'ARS' => [
        'name' => 'Argentine Peso',
        'code' => 'ARS',
        'number' => 32,
        'locations' => [
            "Argentina"
        ]
    ],
    'AMD' => [
        'name' => 'Armenian Dram',
        'code' => 'AMD',
        'number' => 51,
        'locations' => [
            "Armenia"
        ]
    ],
    'AWG' => [
        'name' => 'Aruban Florin',
        'code' => 'AWG',
        'number' => 533,
        'locations' => [
            "Aruba"
        ]
    ],
    'AUD' => [
        'name' => 'Australian Dollar',
        'code' => 'AUD',
        'number' => 36,
        'locations' => [
            "Australia",
            "Christmas Island",
            "Cocos (keeling) Islands (the)",
            "Heard Island And Mcdonald Islands",
            "Kiribati",
            "Nauru",
            "Norfolk Island",
            "Tuvalu"
        ]
    ],
    'AZN' => [
        'name' => 'Azerbaijanian Manat',
        'code' => 'AZN',
        'number' => 944,
        'locations' => [
            "Azerbaijan"
        ]
    ],
    'BSD' => [
        'name' => 'Bahamian Dollar',
        'code' => 'BSD',
        'number' => 44,
        'locations' => [
            "Bahamas (the)"
        ]
    ],
    'BHD' => [
        'name' => 'Bahraini Dinar',
        'code' => 'BHD',
        'number' => 48,
        'locations' => [
            "Bahrain"
        ]
    ],
    'BDT' => [
        'name' => 'Taka',
        'code' => 'BDT',
        'number' => 50,
        'locations' => [
            "Bangladesh"
        ]
    ],
    'BBD' => [
        'name' => 'Barbados Dollar',
        'code' => 'BBD',
        'number' => 52,
        'locations' => [
            "Barbados"
        ]
    ],
    'BYN' => [
        'name' => 'Belarusian Ruble',
        'code' => 'BYN',
        'number' => 933,
        'locations' => [
            "Belarus"
        ]
    ],
    'BZD' => [
        'name' => 'Belize Dollar',
        'code' => 'BZD',
        'number' => 84,
        'locations' => [
            "Belize"
        ]
    ],
    'XOF' => [
        'name' => 'CFA Franc BCEAO',
        'code' => 'XOF',
        'number' => 952,
        'locations' => [
            "Benin",
            "Burkina Faso",
            "CÔte D'ivoire",
            "Guinea-bissau",
            "Mali",
            "Niger (the)",
            "Senegal",
            "Togo"
        ]
    ],
    'BMD' => [
        'name' => 'Bermudian Dollar',
        'code' => 'BMD',
        'number' => 60,
        'locations' => [
            "Bermuda"
        ]
    ],
    'INR' => [
        'name' => 'Indian Rupee',
        'code' => 'INR',
        'number' => 356,
        'locations' => [
            "Bhutan",
            "India"
        ]
    ],
    'BTN' => [
        'name' => 'Ngultrum',
        'code' => 'BTN',
        'number' => 64,
        'locations' => [
            "Bhutan"
        ]
    ],
    'BOB' => [
        'name' => 'Boliviano',
        'code' => 'BOB',
        'number' => 68,
        'locations' => [
            "Bolivia (plurinational State Of)"
        ]
    ],
    'BOV' => [
        'name' => 'Mvdol',
        'code' => 'BOV',
        'number' => 984,
        'locations' => [
            "Bolivia (plurinational State Of)"
        ]
    ],
    'BAM' => [
        'name' => 'Convertible Mark',
        'code' => 'BAM',
        'number' => 977,
        'locations' => [
            "Bosnia And Herzegovina"
        ]
    ],
    'BWP' => [
        'name' => 'Pula',
        'code' => 'BWP',
        'number' => 72,
        'locations' => [
            "Botswana"
        ]
    ],
    'NOK' => [
        'name' => 'Norwegian Krone',
        'code' => 'NOK',
        'number' => 578,
        'locations' => [
            "Bouvet Island",
            "Norway",
            "Svalbard And Jan Mayen"
        ]
    ],
    'BRL' => [
        'name' => 'Brazilian Real',
        'code' => 'BRL',
        'number' => 986,
        'locations' => [
            "Brazil"
        ]
    ],
    'BND' => [
        'name' => 'Brunei Dollar',
        'code' => 'BND',
        'number' => 96,
        'locations' => [
            "Brunei Darussalam"
        ]
    ],
    'BGN' => [
        'name' => 'Bulgarian Lev',
        'code' => 'BGN',
        'number' => 975,
        'locations' => [
            "Bulgaria"
        ]
    ],
    'BIF' => [
        'name' => 'Burundi Franc',
        'code' => 'BIF',
        'number' => 108,
        'locations' => [
            "Burundi"
        ]
    ],
    'CVE' => [
        'name' => 'Cabo Verde Escudo',
        'code' => 'CVE',
        'number' => 132,
        'locations' => [
            "Cabo Verde"
        ]
    ],
    'KHR' => [
        'name' => 'Riel',
        'code' => 'KHR',
        'number' => 116,
        'locations' => [
            "Cambodia"
        ]
    ],
    'XAF' => [
        'name' => 'CFA Franc BEAC',
        'code' => 'XAF',
        'number' => 950,
        'locations' => [
            "Cameroon",
            "Central African Republic (the)",
            "Chad",
            "Congo (the)",
            "Equatorial Guinea",
            "Gabon"
        ]
    ],
    'CAD' => [
        'name' => 'Canadian Dollar',
        'code' => 'CAD',
        'number' => 124,
        'locations' => [
            "Canada"
        ]
    ],
    'KYD' => [
        'name' => 'Cayman Islands Dollar',
        'code' => 'KYD',
        'number' => 136,
        'locations' => [
            "Cayman Islands (the)"
        ]
    ],
    'CLP' => [
        'name' => 'Chilean Peso',
        'code' => 'CLP',
        'number' => 152,
        'locations' => [
            "Chile"
        ]
    ],
    'CLF' => [
        'name' => 'Unidad de Fomento',
        'code' => 'CLF',
        'number' => 990,
        'locations' => [
            "Chile"
        ]
    ],
    'CNY' => [
        'name' => 'Yuan Renminbi',
        'code' => 'CNY',
        'number' => 156,
        'locations' => [
            "China"
        ]
    ],
    'COP' => [
        'name' => 'Colombian Peso',
        'code' => 'COP',
        'number' => 170,
        'locations' => [
            "Colombia"
        ]
    ],
    'COU' => [
        'name' => 'Unidad de Valor Real',
        'code' => 'COU',
        'number' => 970,
        'locations' => [
            "Colombia"
        ]
    ],
    'KMF' => [
        'name' => 'Comoro Franc',
        'code' => 'KMF',
        'number' => 174,
        'locations' => [
            "Comoros (the)"
        ]
    ],
    'CDF' => [
        'name' => 'Congolese Franc',
        'code' => 'CDF',
        'number' => 976,
        'locations' => [
            "Congo (the Democratic Republic Of The)"
        ]
    ],
    'NZD' => [
        'name' => 'New Zealand Dollar',
        'code' => 'NZD',
        'number' => 554,
        'locations' => [
            "Cook Islands (the)",
            "New Zealand",
            "Niue",
            "Pitcairn",
            "Tokelau"
        ]
    ],
    'CRC' => [
        'name' => 'Costa Rican Colon',
        'code' => 'CRC',
        'number' => 188,
        'locations' => [
            "Costa Rica"
        ]
    ],
    'HRK' => [
        'name' => 'Kuna',
        'code' => 'HRK',
        'number' => 191,
        'locations' => [
            "Croatia"
        ]
    ],
    'CUP' => [
        'name' => 'Cuban Peso',
        'code' => 'CUP',
        'number' => 192,
        'locations' => [
            "Cuba"
        ]
    ],
    'CUC' => [
        'name' => 'Peso Convertible',
        'code' => 'CUC',
        'number' => 931,
        'locations' => [
            "Cuba"
        ]
    ],
    'ANG' => [
        'name' => 'Netherlands Antillean Guilder',
        'code' => 'ANG',
        'number' => 532,
        'locations' => [
            "CuraÇao",
            "Sint Maarten (dutch Part)"
        ]
    ],
    'CZK' => [
        'name' => 'Czech Koruna',
        'code' => 'CZK',
        'number' => 203,
        'locations' => [
            "Czech Republic (the)"
        ]
    ],
    'DKK' => [
        'name' => 'Danish Krone',
        'code' => 'DKK',
        'number' => 208,
        'locations' => [
            "Denmark",
            "Faroe Islands (the)",
            "Greenland"
        ]
    ],
    'DJF' => [
        'name' => 'Djibouti Franc',
        'code' => 'DJF',
        'number' => 262,
        'locations' => [
            "Djibouti"
        ]
    ],
    'DOP' => [
        'name' => 'Dominican Peso',
        'code' => 'DOP',
        'number' => 214,
        'locations' => [
            "Dominican Republic (the)"
        ]
    ],
    'EGP' => [
        'name' => 'Egyptian Pound',
        'code' => 'EGP',
        'number' => 818,
        'locations' => [
            "Egypt"
        ]
    ],
    'SVC' => [
        'name' => 'El Salvador Colon',
        'code' => 'SVC',
        'number' => 222,
        'locations' => [
            "El Salvador"
        ]
    ],
    'ERN' => [
        'name' => 'Nakfa',
        'code' => 'ERN',
        'number' => 232,
        'locations' => [
            "Eritrea"
        ]
    ],
    'ETB' => [
        'name' => 'Ethiopian Birr',
        'code' => 'ETB',
        'number' => 230,
        'locations' => [
            "Ethiopia"
        ]
    ],
    'FKP' => [
        'name' => 'Falkland Islands Pound',
        'code' => 'FKP',
        'number' => 238,
        'locations' => [
            "Falkland Islands (the) [malvinas]"
        ]
    ],
    'FJD' => [
        'name' => 'Fiji Dollar',
        'code' => 'FJD',
        'number' => 242,
        'locations' => [
            "Fiji"
        ]
    ],
    'XPF' => [
        'name' => 'CFP Franc',
        'code' => 'XPF',
        'number' => 953,
        'locations' => [
            "French Polynesia",
            "New Caledonia",
            "Wallis And Futuna"
        ]
    ],
    'GMD' => [
        'name' => 'Dalasi',
        'code' => 'GMD',
        'number' => 270,
        'locations' => [
            "Gambia (the)"
        ]
    ],
    'GEL' => [
        'name' => 'Lari',
        'code' => 'GEL',
        'number' => 981,
        'locations' => [
            "Georgia"
        ]
    ],
    'GHS' => [
        'name' => 'Ghana Cedi',
        'code' => 'GHS',
        'number' => 936,
        'locations' => [
            "Ghana"
        ]
    ],
    'GIP' => [
        'name' => 'Gibraltar Pound',
        'code' => 'GIP',
        'number' => 292,
        'locations' => [
            "Gibraltar"
        ]
    ],
    'GTQ' => [
        'name' => 'Quetzal',
        'code' => 'GTQ',
        'number' => 320,
        'locations' => [
            "Guatemala"
        ]
    ],
    'GBP' => [
        'name' => 'Pound Sterling',
        'code' => 'GBP',
        'number' => 826,
        'locations' => [
            "Guernsey",
            "Isle Of Man",
            "Jersey",
            "United Kingdom Of Great Britain And Northern Ireland (the)"
        ]
    ],
    'GNF' => [
        'name' => 'Guinea Franc',
        'code' => 'GNF',
        'number' => 324,
        'locations' => [
            "Guinea"
        ]
    ],
    'GYD' => [
        'name' => 'Guyana Dollar',
        'code' => 'GYD',
        'number' => 328,
        'locations' => [
            "Guyana"
        ]
    ],
    'HTG' => [
        'name' => 'Gourde',
        'code' => 'HTG',
        'number' => 332,
        'locations' => [
            "Haiti"
        ]
    ],
    'HNL' => [
        'name' => 'Lempira',
        'code' => 'HNL',
        'number' => 340,
        'locations' => [
            "Honduras"
        ]
    ],
    'HKD' => [
        'name' => 'Hong Kong Dollar',
        'code' => 'HKD',
        'number' => 344,
        'locations' => [
            "Hong Kong"
        ]
    ],
    'HUF' => [
        'name' => 'Forint',
        'code' => 'HUF',
        'number' => 348,
        'locations' => [
            "Hungary"
        ]
    ],
    'ISK' => [
        'name' => 'Iceland Krona',
        'code' => 'ISK',
        'number' => 352,
        'locations' => [
            "Iceland"
        ]
    ],
    'IDR' => [
        'name' => 'Rupiah',
        'code' => 'IDR',
        'number' => 360,
        'locations' => [
            "Indonesia"
        ]
    ],
    'XDR' => [
        'name' => 'SDR (Special Drawing Right)',
        'code' => 'XDR',
        'number' => 960,
        'locations' => [
            "International Monetary Fund (imf) "
        ]
    ],
    'IRR' => [
        'name' => 'Iranian Rial',
        'code' => 'IRR',
        'number' => 364,
        'locations' => [
            "Iran (islamic Republic Of)"
        ]
    ],
    'IQD' => [
        'name' => 'Iraqi Dinar',
        'code' => 'IQD',
        'number' => 368,
        'locations' => [
            "Iraq"
        ]
    ],
    'ILS' => [
        'name' => 'New Israeli Sheqel',
        'code' => 'ILS',
        'number' => 376,
        'locations' => [
            "Israel"
        ]
    ],
    'JMD' => [
        'name' => 'Jamaican Dollar',
        'code' => 'JMD',
        'number' => 388,
        'locations' => [
            "Jamaica"
        ]
    ],
    'JPY' => [
        'name' => 'Yen',
        'code' => 'JPY',
        'number' => 392,
        'locations' => [
            "Japan"
        ]
    ],
    'JOD' => [
        'name' => 'Jordanian Dinar',
        'code' => 'JOD',
        'number' => 400,
        'locations' => [
            "Jordan"
        ]
    ],
    'KZT' => [
        'name' => 'Tenge',
        'code' => 'KZT',
        'number' => 398,
        'locations' => [
            "Kazakhstan"
        ]
    ],
    'KES' => [
        'name' => 'Kenyan Shilling',
        'code' => 'KES',
        'number' => 404,
        'locations' => [
            "Kenya"
        ]
    ],
    'KPW' => [
        'name' => 'North Korean Won',
        'code' => 'KPW',
        'number' => 408,
        'locations' => [
            "Korea (the Democratic People’s Republic Of)"
        ]
    ],
    'KRW' => [
        'name' => 'Won',
        'code' => 'KRW',
        'number' => 410,
        'locations' => [
            "Korea (the Republic Of)"
        ]
    ],
    'KWD' => [
        'name' => 'Kuwaiti Dinar',
        'code' => 'KWD',
        'number' => 414,
        'locations' => [
            "Kuwait"
        ]
    ],
    'KGS' => [
        'name' => 'Som',
        'code' => 'KGS',
        'number' => 417,
        'locations' => [
            "Kyrgyzstan"
        ]
    ],
    'LAK' => [
        'name' => 'Kip',
        'code' => 'LAK',
        'number' => 418,
        'locations' => [
            "Lao People’s Democratic Republic (the)"
        ]
    ],
    'LBP' => [
        'name' => 'Lebanese Pound',
        'code' => 'LBP',
        'number' => 422,
        'locations' => [
            "Lebanon"
        ]
    ],
    'LSL' => [
        'name' => 'Loti',
        'code' => 'LSL',
        'number' => 426,
        'locations' => [
            "Lesotho"
        ]
    ],
    'ZAR' => [
        'name' => 'Rand',
        'code' => 'ZAR',
        'number' => 710,
        'locations' => [
            "Lesotho",
            "Namibia",
            "South Africa"
        ]
    ],
    'LRD' => [
        'name' => 'Liberian Dollar',
        'code' => 'LRD',
        'number' => 430,
        'locations' => [
            "Liberia"
        ]
    ],
    'LYD' => [
        'name' => 'Libyan Dinar',
        'code' => 'LYD',
        'number' => 434,
        'locations' => [
            "Libya"
        ]
    ],
    'CHF' => [
        'name' => 'Swiss Franc',
        'code' => 'CHF',
        'number' => 756,
        'locations' => [
            "Liechtenstein",
            "Switzerland"
        ]
    ],
    'MOP' => [
        'name' => 'Pataca',
        'code' => 'MOP',
        'number' => 446,
        'locations' => [
            "Macao"
        ]
    ],
    'MKD' => [
        'name' => 'Denar',
        'code' => 'MKD',
        'number' => 807,
        'locations' => [
            "Macedonia (the Former Yugoslav Republic Of)"
        ]
    ],
    'MGA' => [
        'name' => 'Malagasy Ariary',
        'code' => 'MGA',
        'number' => 969,
        'locations' => [
            "Madagascar"
        ]
    ],
    'MWK' => [
        'name' => 'Malawi Kwacha',
        'code' => 'MWK',
        'number' => 454,
        'locations' => [
            "Malawi"
        ]
    ],
    'MYR' => [
        'name' => 'Malaysian Ringgit',
        'code' => 'MYR',
        'number' => 458,
        'locations' => [
            "Malaysia"
        ]
    ],
    'MVR' => [
        'name' => 'Rufiyaa',
        'code' => 'MVR',
        'number' => 462,
        'locations' => [
            "Maldives"
        ]
    ],
    'MRO' => [
        'name' => 'Ouguiya',
        'code' => 'MRO',
        'number' => 478,
        'locations' => [
            "Mauritania"
        ]
    ],
    'MUR' => [
        'name' => 'Mauritius Rupee',
        'code' => 'MUR',
        'number' => 480,
        'locations' => [
            "Mauritius"
        ]
    ],
    'XUA' => [
        'name' => 'ADB Unit of Account',
        'code' => 'XUA',
        'number' => 965,
        'locations' => [
            "Member Countries Of The African Development Bank Group"
        ]
    ],
    'MXN' => [
        'name' => 'Mexican Peso',
        'code' => 'MXN',
        'number' => 484,
        'locations' => [
            "Mexico"
        ]
    ],
    'MXV' => [
        'name' => 'Mexican Unidad de Inversion (UDI)',
        'code' => 'MXV',
        'number' => 979,
        'locations' => [
            "Mexico"
        ]
    ],
    'MDL' => [
        'name' => 'Moldovan Leu',
        'code' => 'MDL',
        'number' => 498,
        'locations' => [
            "Moldova (the Republic Of)"
        ]
    ],
    'MNT' => [
        'name' => 'Tugrik',
        'code' => 'MNT',
        'number' => 496,
        'locations' => [
            "Mongolia"
        ]
    ],
    'MAD' => [
        'name' => 'Moroccan Dirham',
        'code' => 'MAD',
        'number' => 504,
        'locations' => [
            "Morocco",
            "Western Sahara"
        ]
    ],
    'MZN' => [
        'name' => 'Mozambique Metical',
        'code' => 'MZN',
        'number' => 943,
        'locations' => [
            "Mozambique"
        ]
    ],
    'MMK' => [
        'name' => 'Kyat',
        'code' => 'MMK',
        'number' => 104,
        'locations' => [
            "Myanmar"
        ]
    ],
    'NAD' => [
        'name' => 'Namibia Dollar',
        'code' => 'NAD',
        'number' => 516,
        'locations' => [
            "Namibia"
        ]
    ],
    'NPR' => [
        'name' => 'Nepalese Rupee',
        'code' => 'NPR',
        'number' => 524,
        'locations' => [
            "Nepal"
        ]
    ],
    'NIO' => [
        'name' => 'Cordoba Oro',
        'code' => 'NIO',
        'number' => 558,
        'locations' => [
            "Nicaragua"
        ]
    ],
    'NGN' => [
        'name' => 'Naira',
        'code' => 'NGN',
        'number' => 566,
        'locations' => [
            "Nigeria"
        ]
    ],
    'OMR' => [
        'name' => 'Rial Omani',
        'code' => 'OMR',
        'number' => 512,
        'locations' => [
            "Oman"
        ]
    ],
    'PKR' => [
        'name' => 'Pakistan Rupee',
        'code' => 'PKR',
        'number' => 586,
        'locations' => [
            "Pakistan"
        ]
    ],
    'PAB' => [
        'name' => 'Balboa',
        'code' => 'PAB',
        'number' => 590,
        'locations' => [
            "Panama"
        ]
    ],
    'PGK' => [
        'name' => 'Kina',
        'code' => 'PGK',
        'number' => 598,
        'locations' => [
            "Papua New Guinea"
        ]
    ],
    'PYG' => [
        'name' => 'Guarani',
        'code' => 'PYG',
        'number' => 600,
        'locations' => [
            "Paraguay"
        ]
    ],
    'PEN' => [
        'name' => 'Sol',
        'code' => 'PEN',
        'number' => 604,
        'locations' => [
            "Peru"
        ]
    ],
    'PHP' => [
        'name' => 'Philippine Peso',
        'code' => 'PHP',
        'number' => 608,
        'locations' => [
            "Philippines (the)"
        ]
    ],
    'PLN' => [
        'name' => 'Zloty',
        'code' => 'PLN',
        'number' => 985,
        'locations' => [
            "Poland"
        ]
    ],
    'QAR' => [
        'name' => 'Qatari Rial',
        'code' => 'QAR',
        'number' => 634,
        'locations' => [
            "Qatar"
        ]
    ],
    'RON' => [
        'name' => 'Romanian Leu',
        'code' => 'RON',
        'number' => 946,
        'locations' => [
            "Romania"
        ]
    ],
    'RUB' => [
        'name' => 'Russian Ruble',
        'code' => 'RUB',
        'number' => 643,
        'locations' => [
            "Russian Federation (the)"
        ]
    ],
    'RWF' => [
        'name' => 'Rwanda Franc',
        'code' => 'RWF',
        'number' => 646,
        'locations' => [
            "Rwanda"
        ]
    ],
    'SHP' => [
        'name' => 'Saint Helena Pound',
        'code' => 'SHP',
        'number' => 654,
        'locations' => [
            "Saint Helena, Ascension And Tristan Da Cunha"
        ]
    ],
    'WST' => [
        'name' => 'Tala',
        'code' => 'WST',
        'number' => 882,
        'locations' => [
            "Samoa"
        ]
    ],
    'STD' => [
        'name' => 'Dobra',
        'code' => 'STD',
        'number' => 678,
        'locations' => [
            "Sao Tome And Principe"
        ]
    ],
    'SAR' => [
        'name' => 'Saudi Riyal',
        'code' => 'SAR',
        'number' => 682,
        'locations' => [
            "Saudi Arabia"
        ]
    ],
    'RSD' => [
        'name' => 'Serbian Dinar',
        'code' => 'RSD',
        'number' => 941,
        'locations' => [
            "Serbia"
        ]
    ],
    'SCR' => [
        'name' => 'Seychelles Rupee',
        'code' => 'SCR',
        'number' => 690,
        'locations' => [
            "Seychelles"
        ]
    ],
    'SLL' => [
        'name' => 'Leone',
        'code' => 'SLL',
        'number' => 694,
        'locations' => [
            "Sierra Leone"
        ]
    ],
    'SGD' => [
        'name' => 'Singapore Dollar',
        'code' => 'SGD',
        'number' => 702,
        'locations' => [
            "Singapore"
        ]
    ],
    'XSU' => [
        'name' => 'Sucre',
        'code' => 'XSU',
        'number' => 994,
        'locations' => [
            "Sistema Unitario De Compensacion Regional De Pagos 'sucre'"
        ]
    ],
    'SBD' => [
        'name' => 'Solomon Islands Dollar',
        'code' => 'SBD',
        'number' => 90,
        'locations' => [
            "Solomon Islands"
        ]
    ],
    'SOS' => [
        'name' => 'Somali Shilling',
        'code' => 'SOS',
        'number' => 706,
        'locations' => [
            "Somalia"
        ]
    ],
    'SSP' => [
        'name' => 'South Sudanese Pound',
        'code' => 'SSP',
        'number' => 728,
        'locations' => [
            "South Sudan"
        ]
    ],
    'LKR' => [
        'name' => 'Sri Lanka Rupee',
        'code' => 'LKR',
        'number' => 144,
        'locations' => [
            "Sri Lanka"
        ]
    ],
    'SDG' => [
        'name' => 'Sudanese Pound',
        'code' => 'SDG',
        'number' => 938,
        'locations' => [
            "Sudan (the)"
        ]
    ],
    'SRD' => [
        'name' => 'Surinam Dollar',
        'code' => 'SRD',
        'number' => 968,
        'locations' => [
            "Suriname"
        ]
    ],
    'SZL' => [
        'name' => 'Lilangeni',
        'code' => 'SZL',
        'number' => 748,
        'locations' => [
            "Swaziland"
        ]
    ],
    'SEK' => [
        'name' => 'Swedish Krona',
        'code' => 'SEK',
        'number' => 752,
        'locations' => [
            "Sweden"
        ]
    ],
    'CHE' => [
        'name' => 'WIR Euro',
        'code' => 'CHE',
        'number' => 947,
        'locations' => [
            "Switzerland"
        ]
    ],
    'CHW' => [
        'name' => 'WIR Franc',
        'code' => 'CHW',
        'number' => 948,
        'locations' => [
            "Switzerland"
        ]
    ],
    'SYP' => [
        'name' => 'Syrian Pound',
        'code' => 'SYP',
        'number' => 760,
        'locations' => [
            "Syrian Arab Republic"
        ]
    ],
    'TWD' => [
        'name' => 'New Taiwan Dollar',
        'code' => 'TWD',
        'number' => 901,
        'locations' => [
            "Taiwan (province Of China)"
        ]
    ],
    'TJS' => [
        'name' => 'Somoni',
        'code' => 'TJS',
        'number' => 972,
        'locations' => [
            "Tajikistan"
        ]
    ],
    'TZS' => [
        'name' => 'Tanzanian Shilling',
        'code' => 'TZS',
        'number' => 834,
        'locations' => [
            "Tanzania, United Republic Of"
        ]
    ],
    'THB' => [
        'name' => 'Baht',
        'code' => 'THB',
        'number' => 764,
        'locations' => [
            "Thailand"
        ]
    ],
    'TOP' => [
        'name' => 'Pa’anga',
        'code' => 'TOP',
        'number' => 776,
        'locations' => [
            "Tonga"
        ]
    ],
    'TTD' => [
        'name' => 'Trinidad and Tobago Dollar',
        'code' => 'TTD',
        'number' => 780,
        'locations' => [
            "Trinidad And Tobago"
        ]
    ],
    'TND' => [
        'name' => 'Tunisian Dinar',
        'code' => 'TND',
        'number' => 788,
        'locations' => [
            "Tunisia"
        ]
    ],
    'TRY' => [
        'name' => 'Turkish Lira',
        'code' => 'TRY',
        'number' => 949,
        'locations' => [
            "Turkey"
        ]
    ],
    'TMT' => [
        'name' => 'Turkmenistan New Manat',
        'code' => 'TMT',
        'number' => 934,
        'locations' => [
            "Turkmenistan"
        ]
    ],
    'UGX' => [
        'name' => 'Uganda Shilling',
        'code' => 'UGX',
        'number' => 800,
        'locations' => [
            "Uganda"
        ]
    ],
    'UAH' => [
        'name' => 'Hryvnia',
        'code' => 'UAH',
        'number' => 980,
        'locations' => [
            "Ukraine"
        ]
    ],
    'AED' => [
        'name' => 'UAE Dirham',
        'code' => 'AED',
        'number' => 784,
        'locations' => [
            "United Arab Emirates (the)"
        ]
    ],
    'USN' => [
        'name' => 'US Dollar (Next day)',
        'code' => 'USN',
        'number' => 997,
        'locations' => [
            "United States Of America (the)"
        ]
    ],
    'UYU' => [
        'name' => 'Peso Uruguayo',
        'code' => 'UYU',
        'number' => 858,
        'locations' => [
            "Uruguay"
        ]
    ],
    'UYI' => [
        'name' => 'Uruguay Peso en Unidades Indexadas (URUIURUI)',
        'code' => 'UYI',
        'number' => 940,
        'locations' => [
            "Uruguay"
        ]
    ],
    'UZS' => [
        'name' => 'Uzbekistan Sum',
        'code' => 'UZS',
        'number' => 860,
        'locations' => [
            "Uzbekistan"
        ]
    ],
    'VUV' => [
        'name' => 'Vatu',
        'code' => 'VUV',
        'number' => 548,
        'locations' => [
            "Vanuatu"
        ]
    ],
    'VEF' => [
        'name' => 'Bolívar',
        'code' => 'VEF',
        'number' => 937,
        'locations' => [
            "Venezuela (bolivarian Republic Of)"
        ]
    ],
    'VND' => [
        'name' => 'Dong',
        'code' => 'VND',
        'number' => 704,
        'locations' => [
            "Viet Nam"
        ]
    ],
    'YER' => [
        'name' => 'Yemeni Rial',
        'code' => 'YER',
        'number' => 886,
        'locations' => [
            "Yemen"
        ]
    ],
    'ZMW' => [
        'name' => 'Zambian Kwacha',
        'code' => 'ZMW',
        'number' => 967,
        'locations' => [
            "Zambia"
        ]
    ],
    'ZWL' => [
        'name' => 'Zimbabwe Dollar',
        'code' => 'ZWL',
        'number' => 932,
        'locations' => [
            "Zimbabwe"
        ]
    ],
    'XBA' => [
        'name' => 'Bond Markets Unit European Composite Unit (EURCO)',
        'code' => 'XBA',
        'number' => 955,
        'locations' => [
            "Zz01_bond Markets Unit European_eurco"
        ]
    ],
    'XBB' => [
        'name' => 'Bond Markets Unit European Monetary Unit (E.M.U.-6)',
        'code' => 'XBB',
        'number' => 956,
        'locations' => [
            "Zz02_bond Markets Unit European_emu-6"
        ]
    ],
    'XBC' => [
        'name' => 'Bond Markets Unit European Unit of Account 9 (E.U.A.-9)',
        'code' => 'XBC',
        'number' => 957,
        'locations' => [
            "Zz03_bond Markets Unit European_eua-9"
        ]
    ],
    'XBD' => [
        'name' => 'Bond Markets Unit European Unit of Account 17 (E.U.A.-17)',
        'code' => 'XBD',
        'number' => 958,
        'locations' => [
            "Zz04_bond Markets Unit European_eua-17"
        ]
    ],
    'XTS' => [
        'name' => 'Codes specifically reserved for testing purposes',
        'code' => 'XTS',
        'number' => 963,
        'locations' => [
            "Zz06_testing_code"
        ]
    ],
    'XXX' => [
        'name' => 'The codes assigned for transactions where no currency is involved',
        'code' => 'XXX',
        'number' => 999,
        'locations' => [
            "Zz07_no_currency"
        ]
    ],
    'XAU' => [
        'name' => 'Gold',
        'code' => 'XAU',
        'number' => 959,
        'locations' => [
            "Zz08_gold"
        ]
    ],
    'XPD' => [
        'name' => 'Palladium',
        'code' => 'XPD',
        'number' => 964,
        'locations' => [
            "Zz09_palladium"
        ]
    ],
    'XPT' => [
        'name' => 'Platinum',
        'code' => 'XPT',
        'number' => 962,
        'locations' => [
            "Zz10_platinum"
        ]
    ],
    'XAG' => [
        'name' => 'Silver',
        'code' => 'XAG',
        'number' => 961,
        'locations' => [
            "Zz11_silver"
        ]
    ]
];    