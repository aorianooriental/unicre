<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain\Common;

/**
 * Money
 *
 * @package Acoriano\Unicre\Domain\Common
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Money
{
    /**
     * @var float
     */
    private $value;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * Money
     *
     * @param double $value
     * @param Currency $currency
     */
    public function __construct($value, Currency $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    /**
     * Creates a new money value
     *
     * @param float  $value
     * @param string $currencyCode
     *
     * @return Money
     */
    public static function create($value, $currencyCode)
    {
        $money = new Money($value, new Currency($currencyCode));
        return $money;
    }

    /**
     * Get currency used
     *
     * @return Currency
     */
    public function currency()
    {
        return $this->currency;
    }

    /**
     * Money value
     *
     * @return float
     */
    public function value()
    {
        $number = number_format($this->value, 2);
        return (double) $number;
    }

    /**
     * Returns the integer value needed for payment gateway
     *
     * @return int
     */
    public function forPayment()
    {
        $number = $this->value() * 100;
        return (int) $number;
    }
}
