<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain;

/**
 * Transaction
 *
 * @package Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Transaction
{
    /**
     * @var string
     */
    private $transactionId;

    /**
     * @var \DateTimeImmutable
     */
    private $date;

    /**
     * Creates a Transaction
     *
     * @param string $transactionId
     * @param string $date
     */
    public function __construct($transactionId, $date)
    {
        $this->transactionId = $transactionId;
        $this->date = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $date);
    }

    /**
     * Returns the transaction ID
     *
     * @return string
     */
    public function transactionId()
    {
        return $this->transactionId;
    }

    /**
     * Get the transaction date
     *
     * @return \DateTimeImmutable
     */
    public function date()
    {
        return $this->date;
    }


}