<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\Domain;

use Acoriano\Unicre\Domain\Common\Money;

/**
 * Order
 *
 * @package Acoriano\Unicre\Domain
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Order
{
    /**
     * @var string
     */
    private $reference;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @var \DateTimeImmutable
     */
    private $date;

    /**
     * Creates a Order
     *
     * @param string $reference
     * @param Money  $amount
     */
    public function __construct($reference, Money $amount)
    {
        $this->reference = $reference;
        $this->amount = $amount;
        $this->date = new \DateTimeImmutable();
    }

    /**
     * Returns order reference
     *
     * @return string
     */
    public function reference()
    {
        return $this->reference;
    }

    /**
     * Get order amount
     *
     * @return Money
     */
    public function amount()
    {
        return $this->amount;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function date()
    {
        return $this->date;
    }


}