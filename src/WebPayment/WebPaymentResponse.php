<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\WebPayment;

use Acoriano\Unicre\Domain\Common\Url;

/**
 * WebPaymentResponse
 *
 * @package Acoriano\Unicre\WebPayment
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class WebPaymentResponse
{
    /**
     * @var Result
     */
    private $result;

    /**
     * @var string
     */
    private $token;

    /**
     * @var Url
     */
    private $redirectUrl;


    /**
     * Creates a WebPaymentResponse
     *
     * @param Result $result
     * @param string $token
     * @param Url $redirectUrl
     */
    public function __construct(Result $result, $token, Url $redirectUrl)
    {
        $this->result = $result;
        $this->token = $token;
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * Operation result
     *
     * @return Result
     */
    public function result()
    {
        return $this->result;
    }

    /**
     * Payment details token
     *
     * @return string
     */
    public function token()
    {
        return $this->token;
    }

    /**
     * Payment gateway redirect URL
     *
     * @return Url
     */
    public function redirectUrl()
    {
        return $this->redirectUrl;
    }


}