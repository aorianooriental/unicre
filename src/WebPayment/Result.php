<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\WebPayment;

/**
 * Result
 *
 * @package Acoriano\Unicre\WebPayment
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class Result
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $status;

    /**
     * @var null|string
     */
    private $description;

    /**
     * Creates a Result
     *
     * @param string $code
     * @param string $status
     * @param null|string $description
     */
    public function __construct($code, $status, $description = null)
    {
        $this->code = $code;
        $this->status = $status;
        $this->description = $description;
    }

    /**
     * Result operation code
     *
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * Resulting status or short description
     *
     * @return string
     */
    public function status()
    {
        return $this->status;
    }

    /**
     * Resulting operation description or long description
     *
     * @return null|string
     */
    public function description()
    {
        return $this->description;
    }

}