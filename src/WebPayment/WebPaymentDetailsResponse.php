<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\WebPayment;

use Acoriano\Unicre\Domain\Authorization;
use Acoriano\Unicre\Domain\Payment;
use Acoriano\Unicre\Domain\Transaction;

/**
 * WebPaymentDetailsResponse
 *
 * @package Acoriano\Unicre\WebPayment
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class WebPaymentDetailsResponse
{
    /**
     * @var Result
     */
    private $result;

    /**
     * @var Authorization
     */
    private $authorization;

    /**
     * @var Payment
     */
    private $payment;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * Creates a WebPaymentDetailsResponse
     *
     * @param Result        $result
     * @param Authorization $authorization
     * @param Payment       $payment
     * @param Transaction   $transaction
     */
    public function __construct(
        Result $result,
        Authorization $authorization,
        Payment $payment,
        Transaction $transaction
    )
    {
        $this->result = $result;
        $this->authorization = $authorization;
        $this->payment = $payment;
        $this->transaction = $transaction;
    }

    /**
     * Get response result status
     *
     * @return Result
     */
    public function result()
    {
        return $this->result;
    }

    /**
     * Payment authorization
     *
     * @return Authorization
     */
    public function authorization()
    {
        return $this->authorization;
    }

    /**
     * Get payment
     *
     * @return Payment
     */
    public function payment()
    {
        return $this->payment;
    }

    /**
     * Provider transaction info
     *
     * @return Transaction
     */
    public function transaction()
    {
        return $this->transaction;
    }

}