<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre\WebPayment;

use Acoriano\Unicre\Domain\Buyer;
use Acoriano\Unicre\Domain\Common\Language;
use Acoriano\Unicre\Domain\Common\Url;
use Acoriano\Unicre\Domain\Order;
use Acoriano\Unicre\Domain\Payment;

/**
 * WebPaymentRequest
 *
 * @package Acoriano\Unicre\WebPayment
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
class WebPaymentRequest
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var Payment
     */
    private $payment;

    /**
     * @var Url
     */
    private $returnUrl;

    /**
     * @var Url
     */
    private $cancelUrl;

    /**
     * @var string
     */
    private $securityMode = 'SSL';

    /**
     * @var Language
     */
    private $language;

    private $recurring = [
        'amount' => '',
        'billingCycle' => '',
        'billingLeft' => ''
    ];

    /**
     * @var Buyer|null
     */
    private $buyer;

    /**
     * Creates a WebPaymentRequest
     *
     * @param string $contract
     * @param Order $order
     */
    public function __construct($contract, Order $order)
    {
        $this->order = $order;
        $this->payment = new Payment($contract, $order->amount());
        $this->language = new Language(Language::ENGLISH);
    }

    /**
     * Payment request order
     *
     * @return Order
     */
    public function order()
    {
        return $this->order;
    }

    /**
     * Request payment
     *
     * @return Payment
     */
    public function payment()
    {
        return $this->payment;
    }

    /**
     * Set the return/success URL
     *
     * @param Url $url
     *
     * @return WebPaymentRequest
     */
    public function setReturnUrl(Url $url)
    {
        $this->returnUrl = $url;
        return $this;
    }

    /**
     * Success/Return URL
     *
     * @return Url
     */
    public function returnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * Set the cancel URL
     *
     * @param Url $url
     *
     * @return WebPaymentRequest
     */
    public function setCancelUrl(Url $url)
    {
        $this->cancelUrl = $url;
        return $this;
    }

    /**
     * Cancel URL
     *
     * @return Url
     */
    public function cancelUrl()
    {
        return $this->cancelUrl;
    }

    /**
     * Security mode
     *
     * @return string
     */
    public function securityMode()
    {
        return $this->securityMode;
    }

    /**
     * Payment recurring information
     *
     * @return array
     */
    public function recurring()
    {
        return $this->recurring;
    }

    /**
     * Payment language
     *
     * @return Language
     */
    public function language()
    {
        return $this->language;
    }

    /**
     * Change the current payment language
     *
     * @param Language $language
     *
     * @return WebPaymentRequest
     */
    public function changeLanguage(Language $language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * Set the buyer for this payment request
     *
     * @param Buyer $buyer
     *
     * @return WebPaymentRequest
     */
    public function setBuyer(Buyer $buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * @return Buyer|null
     */
    public function buyer()
    {
        return $this->buyer;
    }

    /**
     * Check if current payment has a buyer
     *
     * @return bool
     */
    public function hasBuyer()
    {
        return $this->buyer instanceof Buyer;
    }

}