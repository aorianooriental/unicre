<?php

/**
 * This file is part of acoriano/unicre package
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre;

/**
 * Defines an Acoriano\Unicre Exception
 *
 * @package Acoriano\Unicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
interface Exception
{

}