<?php

/**
 * This file is part of Unicre
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Acoriano\Unicre;

use Acoriano\Unicre\Domain\Payment;
use Acoriano\Unicre\Domain\Transaction;
use Acoriano\Unicre\Exception\WebPaymentResponse\InternalErrorException;
use Acoriano\Unicre\Exception\WebPaymentResponse\InvalidTransactionException;
use Acoriano\Unicre\Exception\WebPaymentResponse\TransactionRefusedException;
use Acoriano\Unicre\WebPayment\WebPaymentDetailsResponse;
use Acoriano\Unicre\WebPayment\WebPaymentRequest;
use Acoriano\Unicre\WebPayment\WebPaymentResponse;

/**
 * WebPaymentGetaway
 *
 * @package Acoriano\Unicre
 * @author  Filipe Silva <silvam.filipe@gmail.com>
 */
interface WebPaymentGetaway
{

    /**
     * Does a web payment request
     *
     * @param WebPaymentRequest $request
     *
     * @return WebPaymentResponse
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    public function doWebPayment(WebPaymentRequest $request);

    /**
     * Retrieves the details of the payment with provided token
     *
     * @param string $token
     *
     * @return WebPaymentDetailsResponse
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    public function getWebPaymentDetails($token);

    /**
     * Requests validation of an accepted authorization
     *
     * @param Transaction $transaction
     * @param Payment $payment
     *
     * @return mixed
     *
     * @throws InternalErrorException
     * @throws TransactionRefusedException
     * @throws InvalidTransactionException
     */
    public function doCapture(Transaction $transaction, Payment $payment);
}